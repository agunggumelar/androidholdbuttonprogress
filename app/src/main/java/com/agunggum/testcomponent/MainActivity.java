package com.agunggum.testcomponent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final int TIMER_LENGTH = 3;

    Button btnPress;
    private TimerView mTimerView;
    long down,up;
    private CountDownTimer cTimer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPress = (Button) findViewById(R.id.btnSubmit);
        mTimerView = (TimerView) findViewById(R.id.progressBar);

        // test

        // penambahan fitur 1 di versi 1.8

        // penambahan fitur 2 di versi 1.8 -- bug fixing

        // penambahan fitur 3 di versi 1.8 -- bug fixing

        // penambahan fitur 1 di versi 1.9

        // penambahan fitur 2 di versi 1.9 -- bug fix

        // penambahan fitur 3 di versi 1.9 -- bug fix

        // penambahan fitur 4 di versi 1.9

        // penambahan fitur 5 di versi 1.9

        // penambahan fitur 1 di versi 2.0

        // penambahan fitur 2 di versi 2.0

        // penambahan fitur 3 di versi 2.0

        // penambahan fitur 1 di versi 2.1 -- bug fix lagi 2x

        // penambahan fitur 2 di versi 2.1 -- bug fix versi

        // penambahan fitur 1 di versi 2.2

        // penambahan fitur 2 di versi 2.2

        // penambahan fitur 3 di versi 2.2

        // penambahan fitur 4 di versi 2.2


        /*
        Test this is new feature
        tambah dikit
         */

        btnPress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_DOWN :
                        mTimerView.start(TIMER_LENGTH);
                        down=System.currentTimeMillis();
                        cTimer = new CountDownTimer(3100, 1000) {
                            public void onTick(long millisUntilFinished) {
                                System.out.println(millisUntilFinished/1000);
                                if (down < 3100) {
                                    Toast.makeText(MainActivity.this, "Kurang dari 3 detik", Toast.LENGTH_SHORT).show();
                                    mTimerView.stop();
                                    cTimer.cancel();
                                }
                            }
                            public void onFinish() {
                                Toast.makeText(MainActivity.this, "Sukses", Toast.LENGTH_SHORT).show();
                                submit();
                                //mTimerView.stop();
                            }
                        };
                        cTimer.start();
                        break;
                    case MotionEvent.ACTION_UP :
                        up=System.currentTimeMillis();
                        cTimer.cancel();
                        if (up - down < 3000) {
                            Toast.makeText(MainActivity.this, "Kurang dari 3 detik", Toast.LENGTH_SHORT).show();
                        }
                        mTimerView.stop();
                        break;
                }
                return false;
            }
        });

    }

    private void submit() {
        Intent intent = new Intent(this, SuccessActivity.class);
        startActivity(intent);
        //finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimerView.init();
    }

    @Override
    protected void onPause() {
        mTimerView.stop();
        if(cTimer!=null) {
            cTimer.cancel();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mTimerView.stop();
        if(cTimer!=null) {
            cTimer.cancel();
        }

        super.onDestroy();

    }
}